# Being Good
<img src="/assets/images/emperors-new-groove.jpg" />

***O*n one shoulder**, there is an angel and, on the other shoulder, a devil. They are the source of all our thoughts and inclinations. Often both sides "weigh-in", and one or neither wins out.

We can usually intuitively discern whether our thoughts feel devilish in nature, but it's not always so clear. Our own intelligence turns on us at times. Like a fungus, spores of deception are omnipresent in the air we breathe. A lie can go from pin to fruit overnight, and it spreads even more spores capable of indefinite survival in the vaccum of the subconscious.

> We don't even know when we lie to ourselves.

There is one obvious solution to this problem: **a lie detector test**. But how? How can we detect our own self-deceit?

Well, I don't know. But, imagine we can determine the source of a thought&ndash; it's origin. For humans, I see exactly three elementary motivators:

1. Health
2. Power
3. Pleasure

Even after enumerating 50 different motiviators, I believe they all can be boiled down to three. Let's break them down.

## Health

To be healthy seems quite similar to the ideas of power, security, longevity, influence, potency, etc, but I find usefulness in the distinction. Health of body, mind, and soul, to me, transcends the limits of power and reaches a kind of communion with the spirit, of which power is merely a component. You may prefer a different term such as self-actualization.

It is most certainly a motivator from the **angel**.


## Power

Everyone has his purpose, which can only be achieved through power. It seems his duty to the world and God to achieve his purpose through ethical acquisition and use of power. Therefore, the **angel** advocates it.

However, with power, you can easily achieve the desires of the **devil**, so he also influences you through this core motivation.


## Pleasure

The pleasures of life are many, and naturally occur on the path laid by angels. However, overindulgence is the ultimate goal of the **devil**. You should be wary of any thoughts that are motivated primarily by the desire for pleasure.

Pleasure can take many forms, so be mindful that self-destructive habits and other vanities may fall in this category.

The way it goes.

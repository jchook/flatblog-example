# How Much Will Bitcoin Be Worth?

We have to look at this objectively. To predict the eventual ceiling of the Bitcoin boom is to understand exactly what Bitcoin is and where it's going.

## What is Bitcoin?

It's hard to find a good comparison to Bitcoin because it's so novel. Many are calling it "digital gold", though the comparison is superficial at best.

Gold has intrinsic value.

* **It's shiny.** About [52%](http://www.numbersleuth.org/worlds-gold/) of above-ground gold is used for jewelry.
* **It's useful.** Medicine, electronics, astronautics, and construction all have uses for this reflective, malleable, conductive, and corrosion-resistant metal.
* **It's standard.** Official holdings and investments account for 32% of mined gold.

Many experts agree that Bitcoin does **not** have intrinsic value. It may not be shiny or conductive, but no one can deny its usefulness as the worlds most popular decentralized currency. In fact, this is [its stated intent](https://bitcoin.org/bitcoin.pdf).

Fiat money (e.g. the US dollar) is also intrinsically valueless, yet has the added problem of being controlled by a central authority, usually a national government. With Bitcoin, the central authority is mathematics, and its governing bodies are the participants.

* **It's free as in speech and as in beer.** Bitcoin is an open technology that anyone can use, implement, alter, and distribute without charge.
* **It's politically neutral.** Bitcoin is truly international.
* **It's inherently democratic.** Changes to Bitcoin happen only through voluntary user adoption.


## Where is Bitcoin going?

One day Bitcoin will be a collector's item.

The global Bitcoin mining effort already [costs 10% of what it yields](https://digiconomist.net/bitcoin-energy-consumption), at over $1Bn/year in electricity, not counting the externalized [environmental](https://arstechnica.com/tech-policy/2017/12/bitcoins-insane-energy-consumption-explained/) [costs](http://money.cnn.com/2017/12/07/technology/bitcoin-energy-environment/index.html?iid=EL). The prices of Bitcoin transaction fees have [skyrocketed from a nickel to $20](https://bitinfocharts.com/comparison/bitcoin-transactionfees.html) in under a year&mdash; a direct result of network congestion as the blockchain is throttled at 1MB every 10 minutes. [Forks were attempted to solve the inherent bandwidth problem](https://motherboard.vice.com/en_us/article/a374x8/bitcoin-segwit2x-canceled-fork), but none of them made it into production. The Bitcoin community remains divided and lacks the central leadership needed to make decisions in times where doing nothing spells disaster.

Other cryptocurrencies with better efficiency and scalability (see [Dash](https://dashpay.atlassian.net/wiki/spaces/DOC/pages/1146914/What+is+Dash) or [Chia](http://chia.network/)) will out-muscle Bitcoin's popularity as the blockchain begins to experience full load and crumble under its own weight. Competing coins tied to resources with intrinsic value ([such as your personal information](http://bitclave.com/)) will form deeper anchors in the global economy and may also gain market attention. Entirely new applications of the emerging decentralization technologies will become indispensable and commonplace.

> Bitcoin is a bud that will flower, fade, and whose stem will yield the magnificent fruit of decentralization.




---

- [World currency infographic](https://ei.marketwatch.com/Multimedia/2017/11/28/Photos/NS/MW-FZ157_allthe_20171128163701_NS.png?uuid=44e9a602-d484-11e7-b2b7-9c8e992d421e)


Random idea... prediction networks on Ethereum blockchain

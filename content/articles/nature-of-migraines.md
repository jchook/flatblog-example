---
title: 'The Nature of Migraines: An Interview With My Wife'
author: 'Jan Uecker'
---

> Instinct is something which transcends knowledge. We have, undoubtedly, certain finer fibers that enable us to perceive truths when logical deduction, or any other willful effort of the brain, is futile. &mdash;Nicola Tesla

After finally settling into our living room after a exceedingly long day of errands, family visits, and a sobering status checks of the twenty or so half-finished ‘weekend projects’ we’ve accumulated in recent months my wife, Maggie, with a depressing look of futility tells me, “I’m pretty sure I'm going to have a migraine in the next few days.” Sure enough, the next day she left work early to tend to a rapidly progressing migraine in an effort to keep it from spiraling out of control.

I’ve learned to trust her on these things. She’s had migraines since the age of thirteen, and now, at thirty-five, can claim well over two decades of unwanted experience with the subject. Her false positive rate is exceedingly low at this point.

As a teenager, I too used to get them on a fairly regular basis and remember often knowing exactly when one was coming. Interestingly though I don't recall ever knowing what exactly I based my foresight on. Maybe it was something I ate? Or some particular stressful activity or event? Who knows. I do remember not really caring; the number one objective was to try and mitigate, or at least brace for, the the coming storm, not embark on a study of localized wind patterns and barometric pressure shifts.

This is one of the fundamental problems I anecdotally hear reported from other migraineurs; Its certainly possible to develop a sort of gut instinct regarding the prediction of ones own migraines (though it can take years) but much more difficultly to pin point the causes. Interestingly, I can often sense when Maggie is going to get a migraine, or at the least I'm not surprised when she tells me. Which is probably just a by-product of living with someone for an extended period and the natural hive-mind it fosters. Over the years I’ve also gained a lot of observational knowledge regarding the specifics of some of her triggers and signs; everything from what we had for dinner to how much the dog is barking.

With that in mind I sat down with Maggie to ask her some questions regarding her migraine experiences, the development of her own predictive instincts, and how accurate she thinks some of my observations are. Here are some excerpts of the more interesting segments:

<!--
//Need to merge with interview notes.
Maggie:
Me:
-->

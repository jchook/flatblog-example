# Hello

***T*his is a blog that was conceived and born with love.** It can do anything, just like our sons and daughters. It is quick like Bolt and sturdy like Churchill, yet curious like Fuller and experimental like Faraday. As all good art, it stands a combination of opposites.

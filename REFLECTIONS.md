Here's the issue with build-first mentality:

- When you save a file, e.g. typography.scss, how do you know to build
  index.scss? You don't. So you either have to store some association of files
  to build when files or saved or you have to take some other approach.
- Also, when you save a scss file, the corresponding articles are not updated.


So what if we did this for dev...

The dev server is run in PHP, and when you access the file, it builds the file.

The naive solution is fine! It could be improved with some kind of cache but
immediately it's great.

<?php

use Flatblog\Site;

class MySite extends Site
{
	private static ?self $instance = null;

	public static function getInstance(): self
	{
		return self::$instance = self::$instance
			?? self::fromPath(dirname(__DIR__));
	}
}


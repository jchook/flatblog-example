<?php

require_once __DIR__ . '/bootstrap.php';

$uri = $_SERVER['REQUEST_URI'] ?? '/';
MySite::getInstance()->buildUri($uri);

// Made for use with php -S
return false;

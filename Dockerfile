FROM php:8.1-alpine

RUN apk --no-cache add wget sassc \
  && wget -qO /usr/bin/composer https://getcomposer.org/download/latest-2.2.x/composer.phar \
  && chmod +x /usr/bin/composer \
  && apk del wget \
  && apk add --no-cache --virtual .build-deps \
    autoconf \
    g++ \
    make \
    wget \
  && apk add --no-cache yaml-dev \
  && pecl install yaml \
  && docker-php-ext-enable yaml \
  && apk del .build-deps

RUN apk --no-cache add fd just

VOLUME /packages
WORKDIR /packages/example

CMD ["/bin/sh", "-c", "composer install && bin/dev"]


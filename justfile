default:
  just --list
build:
  docker-compose exec dev bin/build
logs:
  docker-compose logs -f
sh:
  docker-compose exec dev sh
up:
  docker-compose up --build -d

